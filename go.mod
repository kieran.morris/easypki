module gitlab.com/kieran.morris/easypki

go 1.14

require (
	github.com/boltdb/bolt v1.3.1
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/urfave/cli v1.22.4
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
)
